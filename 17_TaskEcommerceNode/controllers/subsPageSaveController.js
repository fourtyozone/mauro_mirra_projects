const Subscription = require('../models/Subscription')
const path = require('path')

module.exports = async (req, res) => {

    let img = req.files.inputFile
    if(img.size > 10000000000000){
        res.end("Immagine troppo grande!")
    }

    img.mv(path.resolve(__dirname, "..", "public/img", img.name), async (error) => {
        if(!error){
            let nuovoSubscription = {
                subname: req.body.inputName,
                subdesc: req.body.inputDesc,
                subprice: req.body.inputPrice,
                subeta: req.body.inputEta,
                subimage: img.name
            }

            let nuovoSub = await Subscription.create(nuovoSubscription)

            if(!nuovoSub){
                res.end("Errore di inserimento")
            }
            else{
                res.redirect(`/abbonamenti/${nuovoSub._id}`)   
            }
        }
        else{
            res.end("Errore di caricamento")
        }
    })
}