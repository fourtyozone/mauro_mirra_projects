const Subscription = require('../models/Subscription')


module.exports = async (req, res) => {          //Dichiaro la arrow function come asincrona
    // console.log(req.session)

    try {
        let elenco = await Subscription.find({})    //Attendi il risultato della find e non vai avanti finché non risponde!    
        //res.json(elenco)

        res.render('index', {
            elencoabbonamenti: elenco              //Passo un oggetto al mio EJS
        })
    } catch (error) {
        res.json(error)
    }
}