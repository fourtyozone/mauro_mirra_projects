const Subscription = require('../models/Subscription')

module.exports = async (req, res) => {
    try {
        let articolo = await Subscription.findById(req.params.postId)

        res.render('post', {
            subscription: articolo
        })
    } catch (error) {
        res.end("Errore del server (sto nel dettaglio)")
    }
}