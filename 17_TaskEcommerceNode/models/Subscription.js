const mongoose = require('mongoose')
const Schema = mongoose.Schema

const SubscriptionSchema = new Schema(
    {
        subname: String,
        subdesc: String,
        subprice: String,
        subeta: String,
        subimage: String
    }
)

const Subscription = mongoose.model('Subscription', SubscriptionSchema)

module.exports = Subscription