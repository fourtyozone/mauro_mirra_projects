const mongoose = require('mongoose')
const Schema = mongoose.Schema

const OrderSchema = new Schema(
    {
        ordersub: String,
        orderowner: String,
        datePosted: {
            type: Date,
            default: new Date()
        },
        ordermethod: String,
        ordernote: String
    }
)

const Order = mongoose.model('Order', OrderSchema)

module.exports = Order