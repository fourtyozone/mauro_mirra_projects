const bcrypt = require('bcrypt')
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UserSchema = new Schema({
        username: {
            type: String,
            required: true,
            unique: true
        },
        password: {
            type: String,
            required: true
        },
        role: {
            type: String,
            require: true
        }
    })

    

UserSchema.pre('save', function(next){
    let utente = this

    bcrypt.hash(utente.password, 12, (err, passwordHashed) => {
        utente.password = passwordHashed;
        next()
    })

})

const User = mongoose.model('User', UserSchema)
module.exports = User