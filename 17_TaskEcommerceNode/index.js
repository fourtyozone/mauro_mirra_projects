// const express = require('express')
// const bodyParser = require('body-parser')
// const fileUpload = require('express-fileupload')
// const mongoose = require('mongoose')
// const expressSession = require('express-session')

// const app = express()
// app.set('view engine', 'ejs')


// app.use(express.static('public'))
// app.use(bodyParser.json())
// app.use(bodyParser.urlencoded({extender: true}))
// app.use(fileUpload())
// app.use(expressSession({
//     secret: "sonomrmiguardi",
//     resave: false,
//     saveUninitialized: true,
//     cookie: {
//         secure: 'auto',
//         maxAge: 3600000
//     }
// }))


const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const fileUpload = require('express-fileupload')
const expressSession = require('express-session')
const app = express()

app.use(express.static('public'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(fileUpload())
app.set('view engine', 'ejs')
app.use(expressSession({
    secret: "sonomrmiguardi",
    resave: false,
    saveUninitialized: true,
    cookie: {
        secure: 'auto',
        maxAge: 3600000
    }
}))

const port = 4000

//Connessione al DB
const db = mongoose.connect("mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass&directConnection=true&ssl=false", {useNewUrlParser: true}, () => {
    console.log("Sono connesso al nuovo taskSupremoNode!")
})

//Avvio server Express
app.listen(port, () => {
    console.log(`Sono in ascolto sulla porta ${port}`)
})



// const User = require('./models/User')

// User.create({
//     username: {
//         type: ciccio,
//         required: true,
//         unique: true
//     },
//     password: {
//         type: pasticcio,
//         required: true
//     },
//     role: {
//         type: USER,
//         require: true
//     }
// })



//VARIABILI GLOBALI
global.loggato = null;
global.ruolo = null;

app.use("*", (req, res, next) => {
    loggato = req.session.userId
    ruolo = req.session.role
    
    console.log(loggato, ruolo)
    next()
})

////


//ROTTE
const homeController = require('./controllers/homeController')
const registerPageController = require('./controllers/registerPageController')
const registerActionController = require('./controllers/registerActionController')
const loginPageController = require('./controllers/loginPageController')
const loginActionController = require('./controllers/loginActionController')
const logoutController = require('./controllers/logoutController')
const subsPageAddController = require('./controllers/subsPageAddController')
const User = require('./models/User')
const subsDetailController = require('./controllers/subsDetailController')

const subsPageSaveController = require(`./controllers/subsPageSaveController`)


const checkAdminMiddleware = require('./middleware/checkAdminMiddleware')

//
app.get("/", homeController)
app.get("/auth/register", registerPageController)
app.post("/auth/register", registerActionController)
app.get("/auth/login", loginPageController)
app.post("/auth/login", loginActionController)
app.get("/auth/logout", logoutController)
app.get("/abbonamenti/new", checkAdminMiddleware, subsPageAddController)
app.post("/abbonamenti/", subsPageSaveController)
app.get("/abbonamenti/:postId", subsDetailController)
// app.get("/subscriptions/add", checkAdminMiddleware, subsPageAddController)
